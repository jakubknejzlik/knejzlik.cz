var express = require('express');
var swig = require('swig');

var app = new express();
var content = require('./../knejzlik.cz-old/content')

var template = swig.renderFile('./index.html');
swig.setDefaults({ cache: false });

console.log(process.env.NODE_ENV);

app.get('/',function(req,res,next){
	if(process.env.NODE_ENV != 'production'){
		res.send(swig.renderFile('./index.html',content));
	}else{
		res.send(template);		
	}
})

app.use(express.static('.'));

app.listen(process.env.PORT,function(){
	console.log('listening on',process.env.PORT);
});